* Update documentation.
* Fix running of multiple jobs at boot.
* Fix incorrect re-run of jobs.
