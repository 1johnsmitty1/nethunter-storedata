A simple open-source tool for quickly taking and sharing system logs.
Root is required to grab all logs, but you can manually grant access to the main log (Logcat) through ADB:

adb shell pm grant com.tortel.syslog android.permission.READ_LOGS

More details about this can be found on the application's website.

Log types supported:
* Kernel logs (dmsg)
* Last kernel log (last_kmsg, if device supports it)
* Pstore logs
* Main log (Logcat)
* Modem log
* Event logs
* SELinux audit logs

After taking logs, it compresses them into a single zip, and allows you to share or upload them right away - great for helping debug system issues.

New in v2.0.0: Support for viewing the current system logs in near-real time.

You can also include notes within the zip, and append up to 10 characters to the file name.


All the source is available at https://github.com/Tortel/SysLog
Licensed under the GNU General Public License, version 2.

Uses libsuperuser by Jorrit 'Chainfire' Jongma, available at https://github.com/Chainfire/libsuperuser, AndroidX, the Material Design library, and Android-Terminal-Emulator by Jack Palevich.
The log scrubbing utility came from the CyanogenMod bug report tool

This app is built and signed by Kali NetHunter.
